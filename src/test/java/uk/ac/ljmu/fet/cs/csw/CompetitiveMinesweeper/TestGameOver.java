package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper;

import org.junit.Assert;
import org.junit.Test;

import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.MineMap;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.Spot;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers.TestSolver;

public class TestGameOver {
	@Test // (timeout=100)
	public void testIfItDetectsCompletion() {
		boolean endTested = false, runningTested = false; // Tests for proper variation of the method
		do {
			// Defines map
			int rows = 5;
			int cols = 5;
			double mineRatio = 0.8;
			int uidelay = 0;
			MineMap aMap = new MineMap(rows, cols, mineRatio, uidelay); // Creates Map
			TestSolver ts = new TestSolver(); // Our class to tell if this map is complete
			ts.sendMap(aMap); // Send method

			aMap.pickASpot(0, 0); // Picks a spot to start (0,0 being top left corner)
			if (aMap.getPos(0, 0).type == Spot.EXPLODED) { // If 0,0 is a mine, game is over
				endTested = true;
				Assert.assertTrue("Exploded mine but we are still reported as continuing game", ts.isThisTheEnd());	//Expect the game should end - error
			} else {
				runningTested = true;
				Assert.assertFalse("The game is not yet over but the class still thinks so", ts.isThisTheEnd());	//Expect the game should keep running - error
			}
		} while (!(endTested && runningTested));	//Tests both are True
	}

} 
