package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers;

import java.util.Random;

public class RandomMovement extends RandomMove {

	public static Random rng=new Random(); //Generates random number
		
	public static final RandomMovement[] allMoves=new RandomMovement[] {	//Display all possible moves here
			//All possible CHESS movements
			new RandomMovement(-1, 1), new RandomMovement(-1, 0), new RandomMovement(-1, -1), new RandomMovement(0, 1),
			new RandomMovement(0, -1), new RandomMovement(1, 1), new RandomMovement(1, 0), new RandomMovement(1, -1),
	};
	
	public static RandomMovement getARandomMove() {
		return allMoves[rng.nextInt(allMoves.length)];	//Picks random number of the length of the allMoves Array
	}
	
	private RandomMovement(int x, int y) {
		super(x,y);		//Do what RandomMove would do
	}
} 
