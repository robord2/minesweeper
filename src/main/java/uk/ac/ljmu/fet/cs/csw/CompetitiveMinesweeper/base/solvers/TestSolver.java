package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers;

import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.ExploredSpot;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.MineMap;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.Spot;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers.onepriority.CoordinatesForSpot;

public class TestSolver extends AbstractSolver {
	private int currX, currY; // Stores current spot/position

	@Override
	public void run() { // What to do when running
		super.run();
		currX = RandomMovement.rng.nextInt(getMyMap().cols); // Starting location and move around
		currY = RandomMovement.rng.nextInt(getMyMap().rows);
		int plannedX = currX ;
		int plannedY = currY ;
		exploreTheCurrentSpot(plannedX, plannedY) ;
		mainloop: do {
			RandomMovement nextMove = RandomMovement.getARandomMove(); // Gets a random move
			plannedX = currX + nextMove.offsetX; // Do next X move
			plannedY = currY + nextMove.offsetY; // Do next Y move
			
			ExploredSpot currentSpot = getMyMap().getPos(currX, currY);
			
			// Sets area around the current spot to be explored
			int leftColumn = currX - 1, middleColumn = currX, rightColumn = currX + 1, topRow = currY + 1,
					middleRow = currY, bottomRow = currY - 1;
			
			int numberOfHiddenTiles = 0;
			int numberOfFlagTiles = 0;
			int numberOfSafeTiles = 0;
			int numberOfNumberTiles = 0;	
					
			if (getMyMap().checkOutOfRange(plannedY, plannedX)) { // If out of map don't do the move
				continue mainloop; // Will loop, generating another move
			}
			
			// count the number of tiles near current tile
			int n = 0;
			if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
				if (getMyMap().getPos(leftColumn, topRow).nearMineCount >= 1)
					n++;
			}
			if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
				if (getMyMap().getPos(leftColumn, middleRow).nearMineCount >= 1)
					n++;
			}
			if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
				if (getMyMap().getPos(leftColumn, bottomRow).nearMineCount >= 1)
					n++;
			}
			if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
				if (getMyMap().getPos(middleColumn, topRow).nearMineCount >= 1)
					n++;
			}
			if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
				if (getMyMap().getPos(middleColumn, bottomRow).nearMineCount >= 1)
					n++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
				if (getMyMap().getPos(rightColumn, topRow).nearMineCount >= 1)
					n++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
				if (getMyMap().getPos(rightColumn, middleRow).nearMineCount >= 1)
					n++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, bottomRow)) {
				if (getMyMap().getPos(rightColumn, bottomRow).nearMineCount >= 1)
					n++;
			}
			numberOfNumberTiles = n;		
			System.out.println("--------");
			System.out.println(n +"tiles");
			
			// count the number of hidden tiles near current tile
			int h = 0;
			if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
				if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED)
					h++;
			}
			if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
				if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED)
					h++;
			}
			if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
				if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED)
					h++;
			}
			if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
				if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED)
					h++;
			}
			if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
				if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED)
					h++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
				if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED)
					h++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
				if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED)
					h++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, bottomRow)) {
				if (getMyMap().getPos(rightColumn, bottomRow).type == Spot.UNEXPLORED)
					h++;
			}
			numberOfHiddenTiles = h;
			System.out.println(h+"hidden");
			
			// count the number of flags near current tile
			int f = 0;
			if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
				if (getMyMap().getPos(leftColumn, topRow).type == Spot.FLAG)
					f++;
			}
			if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
				if (getMyMap().getPos(leftColumn, middleRow).type == Spot.FLAG)
					f++;
			}
			if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
				if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.FLAG)
					f++;
			}
			if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
				if (getMyMap().getPos(middleColumn, topRow).type == Spot.FLAG)
					f++;
			}
			if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
				if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.FLAG)
					f++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
				if (getMyMap().getPos(rightColumn, topRow).type == Spot.FLAG)
					f++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
				if (getMyMap().getPos(rightColumn, middleRow).type == Spot.FLAG)
					f++;
			}
			if (!getMyMap().checkOutOfRange(rightColumn, bottomRow)) {
				if (getMyMap().getPos(rightColumn, bottomRow).type == Spot.FLAG)
					f++;
			}
			numberOfFlagTiles = f;
			System.out.println(f+"flags");
			
			if (currentSpot.nearMineCount == 1) {
				if (numberOfHiddenTiles == 1) {  
					System.out.println("Works");
					if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
						if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
						if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
						if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
						if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
						if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
						if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
						if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn + 1, bottomRow)) {
						if (getMyMap().getPos(rightColumn + 1, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn + 1, bottomRow);
						}
					}
				}
			}
			
			// Flags hidden tiles around current position value
			
			
			
			//Explores 6's
			if (currentSpot.nearMineCount == 6) {
				if (numberOfHiddenTiles == 6) {  
					if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
						if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
						if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
						if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
						if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
						if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
						if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
						if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn + 1, bottomRow)) {
						if (getMyMap().getPos(rightColumn + 1, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn + 1, bottomRow);
						}
					}
				}
			}
			//Explores 5's
			if (currentSpot.nearMineCount == 5) {
				if (numberOfHiddenTiles == 5) {  
					if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
						if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
						if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
						if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
						if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
						if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
						if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
						if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn + 1, bottomRow)) {
						if (getMyMap().getPos(rightColumn + 1, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn + 1, bottomRow);
						}
					}
				}
			}
			//Explores 4's
			if (currentSpot.nearMineCount == 4) {
				if (numberOfHiddenTiles == 4) {  
					if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
						if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
						if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
						if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
						if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
						if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
						if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
						if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn + 1, bottomRow)) {
						if (getMyMap().getPos(rightColumn + 1, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn + 1, bottomRow);
						}
					}
				}
			}
			
			//Explores 3's
			if (currentSpot.nearMineCount == 3) {
				if (numberOfHiddenTiles == 3) {  
					if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
						if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
						if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
						if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
						if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
						if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
						if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
						if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn + 1, bottomRow)) {
						if (getMyMap().getPos(rightColumn + 1, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn + 1, bottomRow);
						}
					}
				}
			}
			
			//Explore's 2's
			if (currentSpot.nearMineCount == 2) {
				if (numberOfHiddenTiles == 2) {  
					if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
						if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
						if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
						if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
						if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
						if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
						if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
						if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn + 1, bottomRow)) {
						if (getMyMap().getPos(rightColumn + 1, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn + 1, bottomRow);
						}
					}
				}
			}
			
			// Explore 1's
			if (currentSpot.nearMineCount == 1) {
				if (numberOfHiddenTiles == 1) {  
					if (!getMyMap().checkOutOfRange(leftColumn, topRow)) {
						if (getMyMap().getPos(leftColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, middleRow)) {
						if (getMyMap().getPos(leftColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(leftColumn, bottomRow)) {
						if (getMyMap().getPos(leftColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(leftColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, topRow)) {
						if (getMyMap().getPos(middleColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(middleColumn, bottomRow)) {
						if (getMyMap().getPos(middleColumn, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(middleColumn, bottomRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, topRow)) {
						if (getMyMap().getPos(rightColumn, topRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, topRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn, middleRow)) {
						if (getMyMap().getPos(rightColumn, middleRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn, middleRow);
						}
					}
					if (!getMyMap().checkOutOfRange(rightColumn + 1, bottomRow)) {
						if (getMyMap().getPos(rightColumn + 1, bottomRow).type == Spot.UNEXPLORED) {
							getMyMap().flagASpot(rightColumn + 1, bottomRow);
						}
					}
				}
			}
			
			//Checks we have filled the board to end
			if (getMyMap().getExploredAreaSize() == getMyMap().fieldSize) {
				isThisTheEnd();
			}
			
			// We will jump to a spot with one (attempt)
			for (int cc = 0; cc < getMyMap().cols; cc++) {
				for (int rc = 0; rc < getMyMap().rows; rc++) {
					ExploredSpot aSpot = getMyMap().getPos(rc, cc) ;
					if (Spot.SAFE.equals(aSpot.type)) {
						if (aSpot.nearMineCount == 1) {
							exploreTheCurrentSpot(cc, rc);
							System.out.println("find1");
						} else {
							exploreTheCurrentSpot(plannedX, plannedY);
							continue mainloop;
						}
					}
				}
			}
		} while (!isThisTheEnd()) ;
	}
	
	public void exploreTheCurrentSpot(int plannedX, int plannedY) {
		currX = plannedX; // Updates the current spot/position of X
		currY = plannedY; // Updates the current spot/position of Y
		getMyMap().pickASpot(currY, currX); // Explores current spot/position
	}

	public boolean isThisTheEnd() {
	return getMyMap().isEnded(); // getMyMap = The map we created, isEnded = The game ends
	}
}


//AI class =  uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers.TestSolver

