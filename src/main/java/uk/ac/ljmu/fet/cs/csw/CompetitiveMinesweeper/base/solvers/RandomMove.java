package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers;

public class RandomMove {
	public final int offsetX,offsetY;
	
	public RandomMove(int x, int y) {
		offsetX = x;
		offsetY = y;
	}
} 
